package com.github.cuzfrog.utils

private[cuzfrog] object StringProcessing {
  implicit class Regex(sc: StringContext) {
    def r = new util.matching.Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
  }
}