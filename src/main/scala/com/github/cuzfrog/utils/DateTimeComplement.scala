package com.github.cuzfrog.utils

import java.time.LocalDate
import java.time.ZoneId

object DateTimeComplement {
  implicit class ExLocalDate(in: LocalDate) {
    /**
     * Return a span of days from this date to a specified date.
     */
    def spanByDay(untilDateInclusive: LocalDate): Seq[LocalDate] = {
      val period = in.until(untilDateInclusive).getDays
      (0 to period).map {
        plusDays =>
          in.plusDays(plusDays)
      }
    }
    /**
     * Return a span of months from the month of this date to the month of a specified date.
     *  The LocalDate standing for month has the day of 1.
     */
    def spanByMonth(untilDateInclusive: LocalDate): Seq[LocalDate] = {
      val period = in.until(untilDateInclusive).getMonths
      val beginDate = this.firstDayOfTheMonth
      (0 to period).map {
        plusMonths =>
          beginDate.plusMonths(plusMonths)
      }
    }

    def firstDayOfTheMonth: LocalDate = {
      LocalDate.of(in.getYear, in.getMonth, 1)
    }
    
    def lastDayOfTheMonth: LocalDate ={
      in.plusMonths(1).firstDayOfTheMonth.minusDays(1)
    }
  }

  implicit class ExDate(in: java.util.Date) {
    /**
     * Convert java.util.Date to java.time.LocalDate
     */
    def toLocalDate: LocalDate = {
      in.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
    }
  }
}